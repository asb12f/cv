\documentclass[12pt,a4paper]{amsart}
\usepackage[utf8]{inputenc}
\usepackage[left=1in,right=1in,top=1in,bottom=1in]{geometry}
\title{Research \& Teaching Statements}
\author{Andrew Benesh}

\pagestyle{empty}
\begin{document}
\maketitle
\thispagestyle{empty}
\newpage

\section*{Research Statement}

My program of research has two areas of focus. The first focus is on the development and evaluation of training programs for foster parents, with an emphasis on foster child outcomes and relationships between foster parents, foster children, and birth families. 
My second area of focus is on the experiences of youth and families served by the child welfare and juvenile justice systems and how they relate to youth outcomes.
This area includes research using both traditional modeling approaches to describe relationships between variables, and the use of "black box" statistical learning techniques to develop screening and predictive tools.

Around 400,000 youth are placed in foster, kinship, or group homes each year, and both State and Federal laws mandate training and supervision of foster parents, yet little research has examined whether the currently mandated foster parent training programs have any benefits for foster youth.
Past research has found these youth to be at elevated risk for adverse outcomes (homelessness, pregnancy, adult incarceration, etc.) and reduced likelihood of positive outcomes (college acceptance and graduation, employment, marriage, etc.). 
My research in this area emphasizes relational variables that influence long- and short-term developmental outcomes for these youth.
My projects in this area include reviews of the existing research on foster parent training, development of novel foster parent training programs, evaluating the role of foster parent - foster child relationships on foster child outcomes, and developing valid measures of foster parent, foster child, and birth family relationships.

Research projects for my second area currently centers on developing predictive models for use as screening tools in child welfare and juvenile justice practice. 
This research involves using machine learning techniques (random forest modeling, etc.) to analyze large secondary data sets and develop algorithms to identify youth at risk for placement instability, incarceration, and other adverse outcomes.
While these techniques have been widely applied in other social sciences, they are just beginning to see practical application in child welfare and juvenile justice practice.

\section*{Teaching Statement}

\subsection*{Overview}
My teaching experience includes clinical and academic courses for Master's level students, and a variety of advanced undergraduate courses delivered in both online and traditional formats. 
My teaching philosophy centered on scaffolding skill development through low-stakes developmental activities, role-plays, and problem-based learning.
For each topic, key points are listed first and followed with more detailed discussion.

\subsection*{Purpose}
\begin{itemize}
\item Teaching as a strategy for systemic change
\item Developing student skills in real-world applications, communication, community engagement, critical  thinking, and self-reflection
\end{itemize}

I believe teaching is an extension of my role as a family therapist.
In therapy, I work to change the systems individual are embedded in by building new patterns of communication, behavior, and meaning making.
In teaching, I hope to influence the next generation of professionals as a way to change the patterns of communication, behavior, and meaning making in the systems that the families I treat are embedded in.  
To create these changes, my goal is to teach students to apply academic knowledge to real world problems; to communicate effectively with diverse audiences in traditional and modern formats; to engage with diverse communities beyond the campus environment; to critically evaluate values, beliefs, and thoughts; and to foster self-reflection and personal growth.
I believe these skills prepare students to address complex challenges both within professional roles and as citizens in the larger community. 

\subsection*{Process}
\begin{itemize}
\item Modeling skills and knowledge while drawing on student knowledge and experience
\item Practicing skills with ``low-stakes'' in-class activities and experiential assignments
\item Applying skills to real world problems through larger projects or community-based assignments
\end{itemize}
My role is to create contexts for students to build, refine, and reinforce their knowledge and skills.
To enact my role, I model skills through lectures, role plays, or videos, and try to tailor examples to students' interests, needs, and experiences. 
By evoking examples from students' experiences, I help students engage with the course and understand the material.
I use open-source materials when possible, as I believe this increases access for marginalized students, increases accountability, and encourages students to contribute their knowledge and experiences.

Once students are exposed to new skills, I provide progressively independent opportunities to practice skills through formative assignments, including in-class discussions and debates, group activities, structured role-plays, short papers, and experiential activities.
For example, my Diagnosis and Assessment in Family Therapy students develop their clinical interviewing skills throught a series of activities that include semi-structured role plays, recorded practice interviews, observation and coaching of more advanced students in clinical practice, and conducting independent interviews with community members. 
This ``low stakes'' environment allows students to cement understandings, refine skills, find personally meaningful connections, and reflect on their process before real-world practice.
This is also an opportunity for me to work closely with individual students to assess how they are receiving and applying instruction, and to provide constructive feedback.
Students consistently report the feedback they receive on formative assignments is one of the most valuable parts of my courses.

Finally, I provide opportunities to apply skills to real world problems of students' choosing in larger projects or experiential tasks in the community.
Students work more independently, but are still encouraged to consult with me as they develop their skills.
For example, my Research Methods students design small scale research studies, collect data, complete data analysis, and present results to the class. 
My Family Policy students conduct formal Family Impact Analyses and develop multimedia advocacy materials to promote policies they have researched.
As they complete their independent applications, I encourage students to reflect on their experiences, and to practice communication skills by sharing their accomplishments with others. 

\subsection*{Assessment}
\begin{itemize}
\item Multiple Choice testing to measure knowledge retention
\item ``Low stakes'' formative assessments to refine skills
\item Group projects to demonstrate skill mastery and develop collaborative skills
\end{itemize}
Because I conceptualize teaching as a process of developing knowledge, skills, and engagement, I structure assessments around these goals.
To assess acquisition and retention of new knowledge, I use application-oriented multiple choice testing.
To track skill development, students complete ``low stakes'' formative assessment assignments, often
in the form of short papers, presentations, or class activities.
This provides students opportunities to receive feedback on their skills.
Activities are graded using rubrics emphasizing students process in applying skills.
Students also complete evaluative projects.
These require students to practice engagement with peers and the community while demonstrating how they have used feedback from formative assignments to refine their skills, and to present their work to the class.
As with formative assessments, grading uses rubrics emphasizing the process of student's work. 

I vary the format of assignments (tests, papers, and projects) to create opportunities for students to exercise their learning in multiple ways, and to avoid penalizing students who may be weak in some areas.
For larger projects, I incorporate group work to create opportunities to practice the collaborative skills necessary for success in modern workplaces, and to allow students to take on more complex and rewarding projects.
In all papers, projects, and activities, I ask students to reflect on their experiences during the assignment and relate them to their values and beliefs. 
Students report the reflections are helpful as they develop their professional values and identity.

\subsection*{Development and Accomplishments}
\begin{itemize}
\item Refining teaching through student feedback
\item Students successes using course material beyond the classroom
\item Positive course evaluations and student mentorship
\end{itemize}
I measure my success in teaching by my students' accomplishments and feedback.
I routinely experiment with new ways to share information and help refine students skills.
I try to be overt about this process, and encourage students to collaborate with me to achieve course goals.
I believe this shows respect to student's diverse needs and perspectives, and helps them take ownership of course material.

Many students ask me for letters of recommendation, and several have been accepted into Master's and Doctoral programs using projects from my courses in their applications. 
My Family Policy students have been recognized by legislators for their engagement and thoughtful analysis of issues.
I have received positive course evaluations, with median responses always ``very good'' or ``excellent''.
Students report my courses are very difficult, but they find them rewarding and recommend them peers. 
Former students have sought me as a mentor while pursuing their careers goals, because they value the skills they have developed in my courses and my strategies for supporting student growth.

\end{document}