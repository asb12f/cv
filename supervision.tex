\documentclass[12pt]{amsart}

\usepackage[left=1in, right=1in,top=1in, bottom=.9in]{geometry}

\usepackage{hyperref}
\hypersetup{colorlinks = true, linkcolor = blue, urlcolor = blue, citecolor = black}

\bibliographystyle{apacite}
\usepackage{apacite}

\author{Andrew Benesh }
\title{Supervision Philosophy}
\date{}
\pagestyle{empty}
\begin{document}
\maketitle
\thispagestyle{empty}
\newpage
%A Philosophy of Supervision paper is required as part of the 30-hour supervision fundamentals course or 15-hour didactic course. The length of the Philosophy of Supervision paper is generally four single-spaced, typewritten pages or about 1500 words not counting references or appendices.   The paper should be reviewed by the course instructor and feedback should be given to the supervisor candidate. The supervisor candidate should also present the Philosophy of Supervision paper to her/his Approved Supervisor mentor for discussion and feedback. The paper does not need to be submitted to AAMFT.

The following essay represents an attempt to simultaneously convey and integrate a broad, varied, and perhaps contradictory set of ideas, values, and practices into a coherent statement on the philosophy and practice of MFT supervision.  
It is, and will always be, a work in progress.  
This is not meant to diminish the importance or utility of the document, but rather to establish that it is a momentary reflection of the ongoing dialogue between myself, my supervisees, and the nested contexts in which we are embedded.
At the same time, this essay seeks to establish certain rigid truths, limits, and boundaries about my practice of supervision that are expected to be robust regardless of the ongoing internal and external dialogues that guide my supervision process.

\subsection*{What Is Supervision?}
To establish a useful and meaningful philosophy of supervision, I feel it's necessary to address the issue of ontogeny.  
Without knowing what supervision is, it is impossible to say what it ought to be.
As both a therapist and a supervisor, I find the following definition best captures my beliefs about what supervision ``is'':
\begin{quote}
Supervision is a process of collaborative dialogue between supervisors, supervisees, clients, and external systems which intentionally integrates clinical theory, practical skills training, self-of-the-therapist needs, client and therapist safety, and extra-therapeutic factors in the multiple contexts of clinical practice, professional development, and personal experience, and across the progression of each individual case and the supervisor and supervisees' development. 
\end{quote}

This definition is neither universal nor mandatory - I believe that there are facets which are important in supervision which are not included in the definition above, and I believe that supervision doesn't always require all of the elements listed - but the definition captures the four elements that I consider the ethos of supervision - a dialogue between supervisor and supervisee; intentional integration of relevant ideas, skills, and clinical material; overt consideration of the contexts of therapy and supervision; and awareness of developmental progressions and time.

\subsection*{A Process of Collaborative Dialogue}
As a therapist, I practice from an integrative theoretical approach shaped heavily by the ideas of Gregory Bateson, his granddaughter, Mary Catherine Bateson, and Urie Bronfenbrenner \cite{bateson1987angels, bateson2000steps, bateson2009peripheral, bronfenbrenner2005making}.  
In doing so, I marry General Systems Theory with a Social Constructionist variant of post-modernism; this framework allows me to join an appreciation of individuals' and families' unique experiences and meanings with ideas about homeostasis and patterns disruption as a mechanism of change.
I embrace these ideas on an abstract level, and allow the practical application to be dictated by the unique problem and process of therapy with a particular client, much like in the Integrative Problem-Centered Metaframework approach \cite{breunlin2011integrative, pinsof2011integrative}.
This position recognizes that there are necessarily power differentials in therapy, and suggests that therapists be overt in discussing the nature of these power issues with clients.  
The use of power is contingent on contexts - legitimate power (such as state law or ethical standards) is enforced, while more subjective power positions (``expert'' positioning, etc.) are continuously negotiated through the process of therapy.
Client voices are privileged, and the therapist works with the clients to explore and shift patterns, question and re-evaluate meanings, and to set and revise goals.

These values inform my theory and practice of supervision.
Just as therapy exists within the relationship between a therapist and clients, supervision exists within the relationship between a supervisor, supervisee, and clients.  
All roles involved are interdependent, and exert a measure of power over the supervision process; the exertion of power is regulated by the dialogues that exist between each actor.
As a supervisor, I seek to work with my supervisees to minimize power differences in within the system, to recognize where power differences exist, and to question how these power differences affect meaning and patterns throughout the supervisory system.

Collaboration in the supervisory system requires a degree of trust and overtness, which it is the responsibility of the supervisor to cultivate.  
In the first session, issues of power and collaboration are overtly discussed, and a detailed and overt contract is formed with clearly defined roles, expectations of process, evaluation practices, and terminal goals.  
Rather than privilege my own perspective during session, I encourage my supervisee to express their own thoughts, values, and ideas, and help them work within their own framework.
When I choose to inject my own ideas or beliefs, I do so overtly and with the qualification that my supervisee doesn't have to accept them.
Supervision meetings end with a feedback discussion, in which supervisor and supervisee discuss their experience of supervision in regard to topics like helpfulness, safety, progress, and needs.  

There are, however, times when supervisee ideas and beliefs are \emph{not} privileged.
These occur when there are risks to the safety of the client or therapist, when legitimate power requires specific actions (such as statutory abuse reporting), when ethical violations are occurring, or when the therapist fails to attend to issues which may trigger ethical, legal, or safety imperatives (for example, a therapist who refuses to screen couples for domestic violence).
Because collaboration relies on all parties being invested and honest, the supervisor may also choose to act unilaterally or directively if the supervisee is uninvolved in supervision or demonstrates a lack of professional integrity (lying about cases, etc.).

The use of the word ``dialogue'' here encompasses not just face-to-face speech, but the broader notion of interacting sets of roles and ideas.
This allows for supervision to exist not only between a traditionally defined supervisor and supervisee, but between a supervisee and a text or film, or even purely within a supervisee.  
This distinction is important for two reasons.
First, it recognizes that supervision is a continuous relational process, and that the supervisee has the power to choose which ideas to engage with and privilege in the supervisory process.
Second, it allows for the possibility of self-supervision as goal that supervisees might pursue and achieve \cite{ch2}.  

Because I believe supervision is a process of dialogue, I privilege supervision modalities with direct exchange of ideas between supervisor and supervisee.
In practice, this means that case consultation and viewing recorded sessions are the primary modalities of supervision.
Reviewing raw video or audio together allows for a collaborative dialogue about the therapist's process in the session, mutual attendance to opportunities to do things differently, and a conversation that has the time and space to allow each participant to fully share their thoughts and positions.  
Live supervision may still be used, particularly when issues of ethical, legal, or safety concerns, present or therapists are new to practice and need more immediate clinical support.
During live supervision I prefer implementing mid-session breaks or empowering therapists to choose to step out of the room rather than interrupting sessions, unless interruption is necessary for ethical or safety reasons.

\subsection*{Intentional Integration}  
The content of the supervision dialogue can vary greatly.
It may involve reviewing cases, discussing theories of change, examining clinical models, practicing specific skills, formulating new interventions, assessing safety, reflecting on the self-of-the-therapist, managing administrative tasks, and more.  
Much of this content will vary based on the therapist, the clients, and their contexts.  
Unlike many models of supervision, I believe that the type of content in supervision is a secondary concern. 
I believe is more important is that the content is chosen with intentionality, and integrated into the therapeutic process.

In the context of this paper, ``intentionality'' means that content is chosen for supervision for a specific purpose.
The choice of content is also made collaboratively between the supervisor and supervisee (and, to a degree, the client and context).
This differs from more heavily structured models, where content is prescribed ahead of the session, and from more relativistic models where all content is given equal privilege. 
Decisions about which content to include are made continuously during the process of supervision, but most typically reflect themes and patterns that have been privileged in the therapist's work.
In practice, this is often achieved by asking the superviseee for a brief review of their cases since the last meeting.  
The supervisor and supervisee discuss content items which might be productive, and mutually develop a plan for the session.
Additionally, themes and patterns from previous meetings may be discussed and revisited.
The reasoning behind choosing a particular area of content can vary greatly, depending again on the needs of the participants and contexts. 
As discussed previously, the collaborative aspect of this process is bounded by legal, ethical, and safety parameters, and it is the supervisor's prerogative to introduce these content areas even if the supervisee does not wish to attend to them.

This intentionally selected content is then integrated into the supervisee's therapeutic process, with the goal of helping the supervisee more fully develop patterns and meanings.
Integration should be useful to the supervisee, and enhance the quality of the therapy the supervisee provides either directly (through the development of an intervention or treatment plan) or indirectly (such as developing novel case conceptualizations or addressing self-of-the-therapist issues).  
Integration often involves linking divergent content to highlight isomorphic patterns and meanings; for example, a supervisee might be asked to attend to how their own challenges in self-care mirror those of their ``resistant'' client.
In the short term, this may also mean creating ``muddles'' \cite{bateson2000steps} by highlighting inconsistencies in the supervisee's models and practices, by emphasizing the benefits of conflicting courses of action, by suggesting radical changes of perspective, or even using paradoxical suggestions.

Intentionality and integration are established in the process of therapy from the first meeting, and included overtly in the formulation of the written supervision contract.  
Because they are a major mechanism for changing meaning and patterns in supervision, they are addressed in each meeting both in the ongoing process and through the intentional feedback discussion at the end of each meeting.

\subsection*{Multiple Contexts}
A collaborative dialogue that intentionally integrates multiple content areas is useless if it doesn't also attend to the multiple nested contexts it is situated in, as these will attenuate the meanings and patterns that present themselves.
Thus, it is important to maintain contextual awareness throughout the supervision and therapy process.
In my definition, I identify three broad categories of context - clinical practice, professional development, and personal experience - but this is not an exhaustive listing.  
The relevant contexts will vary on a case to case basis, so having an overt conversation about relevant contexts is a necessary part of the collaborative dialogue.

I choose to describe contexts as nested, rather than hierarchical.
This decision reflects a desire to minimize power differentials when possible, and to highlight the supportive potential of these contexts.  
Whereas a hierarchy places successive levels ``above'' previous levels and implies privilege, in a nest each successive layer sets limits on the layers nested within it while in turn providing a safe supportive environment for the previous levels to develop.
This ``supportive structure'' nurtures development rather than impeding it, and provides opportunities to try new things rather than enforcing old standards.

The ``clincial practice'' context in my definition refers to the multiple contexts surrounding the clinical activities of the therapist.  
This includes the therapeutic relationship, the supervisory relationship, the physical locations of therapy, the coordinating service providers (agencies, insurance providers, etc.), regulatory bodies, and any other pragmatic context of therapy.  
The ``professional development'' context refers to the development of the clinician as a therapist.
This includes items like training, skill work, education, establishing professional boundaries, and progressing towards self-supervision.
The ``personal experience'' context refers to the unique lived experiences of each individual in the system.
It may include concerns like race, gender, family-of-origin issues, self-care, developmental stage, or prior lived experience.  

The interactions between these contexts and the patterns and meaning of the selected content are an essential part of the supervision conversation.  
The supervisor and supervisee discuss which contexts have bearing on the supervisory process and content, consider the unique interactions of multiple contexts, and use this knowledge to situate and guide therapy.
It is the role of the supervisor to help supervisees attend to these contextual issues, particularly when supervisees are still novice therapists, and at times to challenge assumptions that supervisors and supervisees make about these contexts.  

\subsection*{Time}
The final consideration in my philosophy of supervision is time.
This refers to the progression of individual cases, the development of the supervisor and supervisee over time, the development of the therapeutic and supervisory relationships, and time on an hourly, chronological, and historical scale.
As a supervisor, I recognize that time itself can be a significant contextual factor, and needs to be accounted for.  
Time is also practical, particularly in the context of therapy where services are billed by the hour.
As with other contextual factors, I address this whenever possible through overt dialogue.
Supervisees are encouraged to express how they are experiencing different time progressions, and how this affects their therapeutic process.  
\newpage
\bibliography{journalrefs}
\end{document}